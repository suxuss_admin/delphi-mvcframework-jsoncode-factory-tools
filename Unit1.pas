unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,JsonDataObjects,U.JSON.CodeFactory;

type
  TForm1 = class(TForm)
    Memo1: TMemo;
    Button1: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
  SSS:TJSONCodeFactory;
BEGIN
  SSS := TJSONCodeFactory.Create(Edit1.Text);
 // SSS.Parse('{data: [{"accessToken": "333","expireIn": 1,"refreshToken": "222"}]}');
  SSS.Parse(Memo1.Text);

  SSS.SaveToFile(ExtractFilePath(ParamStr(0)),Edit2.Text);
  Application.MessageBox('Generated code successfully', 'Successfully', MB_OK + MB_ICONINFORMATION);

 // JSonObject
end;

end.
