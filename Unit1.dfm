object Form1: TForm1
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'JSONCodeFactory V0.1'
  ClientHeight = 858
  ClientWidth = 953
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Segoe UI'
  Font.Style = []
  TextHeight = 15
  object Label1: TLabel
    Left = 735
    Top = 53
    Width = 58
    Height = 15
    Caption = 'Unit name:'
  end
  object Label2: TLabel
    Left = 735
    Top = 8
    Width = 65
    Height = 15
    Caption = 'Class Name:'
  end
  object Memo1: TMemo
    Left = 8
    Top = 8
    Width = 721
    Height = 842
    Lines.Strings = (
      '{'
      '    "errorCode": 0,'
      '    "data": {'
      '        "appid": "500041933",'
      '        "eid": "10603457",'
      '        "username": "'#21608#23567#36842'",'
      '        "userid": "5a41af0ae4b0820e92cd9868",'
      '        "jobNo":"12312",'
      '        "networkid": "58df7e16e4b053f44063edca",'
      '        "deviceId": "15900000000",'
      '        "openid": "5a41b292e4b058cf3d0cf314",'
      '        "ticket": null'
      '    },'
      '    "success": true,'
      '    "error": null'
      '}')
    TabOrder = 0
  end
  object Button1: TButton
    Left = 736
    Top = 106
    Width = 209
    Height = 41
    Caption = 'Generate Code'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Edit1: TEdit
    Left = 736
    Top = 24
    Width = 209
    Height = 23
    TabOrder = 2
    Text = 'TQuireContext'
  end
  object Edit2: TEdit
    Left = 736
    Top = 69
    Width = 209
    Height = 23
    TabOrder = 3
    Text = 'U.Yunzhijia.REST.QuireContext'
  end
end
